Station Météorologique
======================

Par Le Sanglier des Ardennes

Matériels
=========

Arduino :

- Carte Arduino UNO : http://www.gotronic.fr/art-carte-arduino-uno-12420.htm

Shield Grove :

- Module Grove Base Shield SLD01099P : http://www.gotronic.fr/art-module-grove-base-shield-sld01099p-19068.htm

Capteur Grove :

- Capteur de température Grove SEN23292P : http://www.gotronic.fr/art-capteur-de-temperature-grove-sen23292p-18965.htm
- Capteur d'humidité et de T° Grove SEN11301P : http://www.gotronic.fr/art-capteur-d-humidite-et-de-t-grove-sen11301p-18963.htm
- Module baromètre Grove SEN05291P : http://www.gotronic.fr/art-module-barometre-grove-sen05291p-18953.htm
- Horloge temps réel Grove SEN12671P : http://www.gotronic.fr/art-horloge-temps-reel-grove-sen12671p-18984.htm
- Afficheur LCD série Grove LCD23154P : http://www.gotronic.fr/art-afficheur-lcd-serie-grove-lcd23154p-19018.htm

  Désactiver : 

- Capteur de qualité d'air Grove SEN01111P : http://www.gotronic.fr/art-capteur-de-qualite-d-air-grove-sen01111p-18982.htm
Afficheur : 

DEL :

- Led rouge 5 mm Grove COM04054P : http://www.gotronic.fr/art-led-rouge-5-mm-grove-com04054p-19005.htm

Boutton-poussoir : 

- Module bouton Grove COM08211P : http://www.gotronic.fr/art-module-bouton-grove-com08211p-19010.htm

Pile : 

- Pile au lithium éco CR1220 : http://www.gotronic.fr/art-pile-au-lithium-eco-cr1220-5964.htm
