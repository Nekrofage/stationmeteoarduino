/*
  Read meteo file
  
  
 */
#include <SdFat.h>


const uint8_t chipSelect = SS;

SdFat sd;

ArduinoOutStream cout(Serial);

char fileName[] = "METEO.CSV"; // The filename must be in uppercase

#define error(s) sd.errorHalt_P(PSTR(s))

void readFile() {
  float  ctemperature,humidity,pressure ,atm,altitude;
  char comma;
  int i=0;
    
  ifstream sdin(fileName);
  
  if (!sdin.is_open())  {
     error("open");
  }
  
  
  while (sdin >> ctemperature >> comma >> humidity >> comma >> pressure >> comma >> atm >> comma >> altitude) {
    cout << setw(6) << ctemperature << setw(6) << humidity << setw(6) << pressure << atm << setw(6) << altitude << endl;
  }

  if (!sdin.eof()) {
    error("readFile");
  }
}


void setup() {
  Serial.begin(38400);
  while (!Serial) {} 
  
  cout << pstr("ctemperature;humidity;pressure;atm;altitude\n");
  
  while (Serial.read() <= 0) {}
  
  delay(400);  
  

  if (!sd.begin(chipSelect, SPI_HALF_SPEED)) {
    sd.initErrorHalt();
  }
  
  
  readFile();  
  
}
void loop() {}
