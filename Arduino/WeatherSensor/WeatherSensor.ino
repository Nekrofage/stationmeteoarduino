/*
   Weather sensor

 */

#include <DHT.h>
#include <SerialLCD.h>
#include <SoftwareSerial.h>
#include <SdFat.h>
#include <Barometer.h>
#include <Wire.h>
#include <DS1307.h>

// Horloge
DS1307 clock;

// Barometer

float temperature;
float pressure;
float atm;
float altitude;
Barometer myBarometer;

// SD Card
const uint8_t chipSelect = SS;

SdFat sd;

SdFile weatherFile;

#define error(s) sd.errorHalt_P(PSTR(s))

char name[] = "meteo.csv";

#define SDWRITE 1
#define WEATHERSERIALOUTPUT 1
#define CLOCKSERIALOUTPUT 1

// Temperature adnd humidity

#define DHTPIN A0
#define DHTTYPE DHT22   // DHT 22  (AM2302)

int temperaturePin;

int lapse=1000; // duration between temperature readings

float ctemperature;
int B=3975; 
float resistance;

DHT dht(DHTPIN, DHTTYPE);

// Serial LCD : Pin D8
SerialLCD slcd(8,9);

// LED

#define LED  2

// Button
const int buttonPin = 4;
int buttonState = 0; 
int buttonStatus = 0;

void setup() {
    Serial.begin(9600); 

    myBarometer.init();

    dht.begin();

    slcd.begin();
    slcd.print("Meteo");

    delay(400); 

    if (!sd.begin(chipSelect, SPI_HALF_SPEED)) {
        sd.initErrorHalt();
    }   



    pinMode(LED, OUTPUT);

    digitalWrite(LED, HIGH);



    clock.begin();
    clock.fillByYMD(2014,4,20); //Jan 19,2013
    clock.fillByHMS(8,24,30); //15:28 30
    clock.setTime(); //write time to the RTC chip   
   
    pinMode(buttonPin, INPUT);  
    
}

void loop() {
  
  buttonState = digitalRead(buttonPin);
  
    // Temperature and humidity

    float humidity = dht.readHumidity();

    temperaturePin = analogRead(1);

    resistance = (float)(1023 - temperaturePin ) * 10000 / temperaturePin; 
    ctemperature = 1 / (log(resistance / 10000) / B + 1/298.15) - 273.15;

    if( WEATHERSERIALOUTPUT == 1) {
        Serial.print("Temperature : "); 
        Serial.print(ctemperature);
        Serial.write(" ");
        Serial.write(186);
        Serial.print("C \t");


        Serial.print("Humiditee : "); 
        Serial.print(humidity);
        Serial.println(" %");
    }

    // Barometer


    temperature = myBarometer.bmp085GetTemperature(myBarometer.bmp085ReadUT()); //Get the temperature, bmp085ReadUT MUST be called first
    pressure = myBarometer.bmp085GetPressure(myBarometer.bmp085ReadUP());//Get the temperature
    altitude = myBarometer.calcAltitude(pressure); //Uncompensated caculation - in Meters 
    atm = pressure / 101325; 


    if( WEATHERSERIALOUTPUT == 1) {
        Serial.print("Pressure: ");
        Serial.print(pressure, 0); 
        Serial.print(" Pa  ");


        Serial.print("Ralated Atmosphere: ");
        Serial.print(atm, 4);
        Serial.print(" ");   

        Serial.print("Altitude: ");
        Serial.print(altitude, 2);
        Serial.print(" m");

        Serial.println();
        Serial.println();
    }


    // Serial LCD

    slcd.setCursor(0, 0);
    slcd.print("Temp : ");
    slcd.print(ctemperature , 2);
    slcd.print(" c");


    slcd.setCursor(0, 1);
    slcd.print("Humid : ");
    slcd.print(humidity , 2);
    slcd.print(" %");    


    clock.getTime();



    if (CLOCKSERIALOUTPUT == 1) {
        Serial.print("Horloge : ");
        Serial.print(clock.year, DEC);
        Serial.print("-");
        Serial.print(clock.month, DEC); 
        Serial.print("-");
        Serial.print(clock.dayOfMonth, DEC);	
        Serial.print("-");
        Serial.print(clock.hour, DEC);
        Serial.print("-");
        Serial.print(clock.minute, DEC);
        Serial.print("-");
        Serial.print(clock.second, DEC);
        Serial.println(" ");      
    } 

  if (buttonState == HIGH) {   
    if (buttonStatus == 1 ) {
       digitalWrite(LED, LOW);
       Serial.println("No write on SD card");  
       buttonStatus = 0;
    } else {  
       digitalWrite(LED, HIGH);
       Serial.println("Write on SD card"); 
       buttonStatus = 1;
    }     
        // SD Card
        if(SDWRITE == 1 && buttonStatus == 1) {
            // open the file for write at end like the Native SD library
            if (!weatherFile.open(name, O_RDWR | O_CREAT | O_AT_END)) {
                sd.errorHalt("opening weather file for write failed");
            }
            
            weatherFile.print(clock.year, DEC);
            weatherFile.print("-");
            weatherFile.print(clock.month, DEC); 
            weatherFile.print("-");
            weatherFile.print(clock.dayOfMonth, DEC);	
            weatherFile.print("-");
            weatherFile.print(clock.hour, DEC);
            weatherFile.print("-");
            weatherFile.print(clock.minute, DEC);
            weatherFile.print("-");
            weatherFile.print(clock.second, DEC);
            weatherFile.print(";");
    
            weatherFile.print(ctemperature);
            weatherFile.print(";");
            weatherFile.print(humidity);
            weatherFile.print(";");
            weatherFile.print(pressure);
            weatherFile.print(";");
            weatherFile.print(atm);
            weatherFile.print(";");
            weatherFile.print(altitude);
    
            weatherFile.println(" ");
    
            // close the file:
            weatherFile.close();
        }
          
  }        
    
    
    delay(lapse);

}
